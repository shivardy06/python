from setuptools import setup, find_packages

setup(
    name='my_app',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'pytest',
        'flake8',
        # Other dependencies
    ],
)
